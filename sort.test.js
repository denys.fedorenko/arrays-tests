const ArrayLike = require('./index');

describe('Tests for method sort', () => {

  test('1.Instance has not own property sort', () => {
    const arr = new ArrayLike();

    expect(arr.hasOwnProperty('sort')).toBeFalsy();
  });

  test('2.Instance has method sort', () => {
    const arr = new ArrayLike();

    expect(arr.sort).toBeInstanceOf(Function);
  });

  test('3.It provides 2 arguments to callback function', () => {
    const arr = new ArrayLike(1, 2, 3, 4);
    const callback = jest.fn();
    arr.sort(callback);

    expect(callback.mock.calls[0]).toHaveLength(2);
    expect(callback.mock.calls[1]).toHaveLength(2);
    expect(callback.mock.calls[2]).toHaveLength(2);
  });

  test('4.It changes array in the place', () => {
    const arr = new ArrayLike(10, 9, 8, 11, 12, -100, 0);
    arr.sort();
    arr.sort();

    expect(arr[0]).toBe(-100);
    expect(arr[2]).toBe(10);
    expect(arr[6]).toBe(9);
  });

  test('5.It sorts items by Unicode when callback has no arguments', () => {
    const arr1 = new ArrayLike('ab', 'AB', 'aB', 'Ab', 'ua');
    const arr2 = new ArrayLike(9, 2, 211, 40, 200, -200, 0, 330, 110, 3);
    arr1.sort();
    arr2.sort();

    expect(arr1[0]).toBe('AB');
    expect(arr1[2]).toBe('aB');
    expect(arr1[4]).toBe('ua');

    expect(arr2[0]).toBe(-200);
    expect(arr2[4]).toBe(200);
    expect(arr2[9]).toBe(9);
  });

  test('6.It reverse array when callback function returns -1', () => {
    const arr1 = new ArrayLike('srt5', 'tr21', 'ab2', 'avre3');
    const arr2 = new ArrayLike(5, 4, -1, 3, 31);
    arr1.sort(() => -1);
    arr2.sort(() => -1);

    expect(arr1[0]).toBe('avre3');
    expect(arr1[3]).toBe('srt5');

    expect(arr2[0]).toBe(31);
    expect(arr2[2]).toBe(-1);
    expect(arr2[4]).toBe(5);
  });

  test('7.Array isn\'t sorted when callback function returns 0 or 1', () => {
    const arr1 = new ArrayLike(10, 2);
    const arr2 = new ArrayLike(7, 300, 40, 'arr', '1arr');
    arr1.sort(() => 0);
    arr2.sort(() => 1);

    expect(arr1).toEqual(new ArrayLike(10, 2));
    expect(arr2).toEqual(new ArrayLike(7, 300, 40, 'arr', '1arr'));
  });

  test('8.Callback is executed 1 time less than length of array', () => {
    const arr1 = new ArrayLike(7, 300, 40, 'arr', '1arr', -1, 5);
    const callback = jest.fn();
    arr1.sort(callback);

    expect(callback.mock.calls.length).toBe(6)
  });

  test('9.It moves empty values to the end of the array', () => {
    const arr = new ArrayLike(300, -1, -100, 2, 40);
    delete arr[2];
    arr.sort();

    expect(arr[4]).toBeUndefined();
  });

  test('10.It executes callback with non-empty values', () => {
    const arr1 = new ArrayLike(7, undefined, 300, NaN, -1, -100, null);
    delete arr1[2];
    const callback = jest.fn();
    arr1.sort(callback);

    expect(callback).toHaveBeenNthCalledWith(1, NaN, 7);
    expect(callback).toHaveBeenNthCalledWith(2, -1, NaN);
    expect(callback).toHaveBeenNthCalledWith(3, -100, -1);
    expect(callback).toHaveBeenNthCalledWith(4, null, -100);
  });

  test('11.The Length property of sort method is 1', () => {

    expect(ArrayLike.prototype.sort).toHaveLength(1);
  });
});