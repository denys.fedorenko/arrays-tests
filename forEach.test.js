const ArrayLike = require('./index');

describe('Tests for method forEach', () => {

  test('1.Instance has not own property forEach', () => {
    const arr = new ArrayLike();

    expect(arr.hasOwnProperty('forEach')).toBeFalsy();
  });

  test('2.Instance has method forEach', () => {
    const arr = new ArrayLike();

    expect(arr.forEach).toBeInstanceOf(Function);
  });

  test('3.Method returns nothing', () => {
    const arr = new ArrayLike(1, 2, 3, 4);
    const callback = jest.fn(() => 100);

    expect(arr.forEach(callback)).toBeUndefined();
  });

  test('4.It provides 3 arguments to callback function', () => {
    const arr = new ArrayLike(1, 2, 3);
    callback = jest.fn();
    arr.forEach(callback);

    expect(callback.mock.calls[0]).toHaveLength(3);
    expect(callback.mock.calls[1]).toHaveLength(3);
    expect(callback.mock.calls[2]).toHaveLength(3);
  });

  describe('Arguments in callback function', () => {
    test('4.1.First argument is currentValue', () => {
      expect(callback.mock.calls[0][0]).toBe(1);
      expect(callback.mock.calls[1][0]).toBe(2);
      expect(callback.mock.calls[2][0]).toBe(3);
    });

    test('4.2.Second argument is currentIndex', () => {
      expect(callback.mock.calls[0][1]).toBe(0);
      expect(callback.mock.calls[1][1]).toBe(1);
      expect(callback.mock.calls[2][1]).toBe(2);
    });

    test('4.3.Third argument is an array', () => {
      expect(callback.mock.calls[0][2]).toEqual(new ArrayLike(1, 2, 3));
      expect(callback.mock.calls[1][2]).toEqual(new ArrayLike(1, 2, 3));
      expect(callback.mock.calls[0][2]).toEqual(new ArrayLike(1, 2, 3));
    });
  });

  test('5.It executes callback once for each non-empty value', () => {
    const arr = new ArrayLike(1, -4, null, NaN, '2');
    const callback = jest.fn();
    delete arr[1];
    arr.forEach(callback);

    expect(callback).toHaveBeenCalledTimes(4);
    expect(callback.mock.calls[0][0]).toBe(1);
    expect(callback.mock.calls[1][0]).toBe(null);
    expect(callback.mock.calls[2][0]).toBe(NaN);
    expect(callback.mock.calls[3][0]).toBe('2');
  });

  test('6.It executes callback on items with value undefined', () => {
    const arr = new ArrayLike(1, undefined, 3);
    const callback = jest.fn();
    arr.forEach(callback);

    expect(callback.mock.calls[0][0]).toBe(1);
    expect(callback.mock.calls[1][0]).toBeUndefined();
    expect(callback.mock.calls[2][0]).toBe(3);
  });

  test('7.It uses second argument as this', () => {
    const arr = new ArrayLike(1, 2, 3);
    const arr2 = {};
    const callback = jest.fn(function (el, index) {
      this[index] = el;
    });
    arr.forEach(callback, arr2);

    expect(arr2).toEqual({0:1, 1:2, 2:3});
  });

  test('8.It throws error when callback is undefined', () => {
    const arr = new ArrayLike(1, 2, 3);

    expect(() => { arr.forEach() }).toThrow(new TypeError('undefined is not a function'));
  });

  test('9.It executes callback as many times as original array length, if elements were not deleted during iteration', () => {
    const arr = new ArrayLike(1, 2, 3, 4);
    const callback = jest.fn(() => { arr[arr.length] = 10 });
    arr.forEach(callback);

    expect(arr).toEqual([1, 2, 3, 4, 10, 10, 10, 10]);
    expect(callback.mock.calls[0][0]).toBe(1);
    expect(callback.mock.calls[1][0]).toBe(2);
    expect(callback.mock.calls[2][0]).toBe(3);
    expect(callback.mock.calls[3][0]).toBe(4);
    expect(callback.mock.calls[4]).toBeUndefined();
  });

  test('10.The length property of forEach method is 1', () => {
    
    expect(ArrayLike.prototype.forEach).toHaveLength(1);
  });

});