const ArrayLike = require('./index');

describe('Tests for method pop', () => {
  test('1.Instance has not own property pop', () => {
    const arr = new ArrayLike();

    expect(arr.hasOwnProperty('pop')).toBeFalsy();
  });

  test('2.Instance has method pop', () => {
    const arr = new ArrayLike();

    expect(arr.pop).toBeInstanceOf(Function);
  });

  test('3.It returns removed element', () => {
    const arr = new ArrayLike(1, 2, 3);

    expect(arr.pop()).toBe(3);
  });

  test('4.It removes the last element of array', () => {
    const arr1 = new ArrayLike(1, 2, 3);
    const arr2 = new ArrayLike(4, 5, 6, 7, 8);
    arr1.pop();
    arr2.pop();

    expect(arr1[2]).toBeUndefined();
    expect(arr2[4]).toBeUndefined();
  });

  test('5.It returns undefined on empty array', () => {
    const arr = new ArrayLike();

    expect(arr.pop()).toBeUndefined();
  });

  test('6.It dicrements array length by 1', () => {
    const arr1 = new ArrayLike(1, 2, 3, 4);
    const arr2 = new ArrayLike(5, 6, 7, null, undefined, NaN, 100);
    arr1.pop();
    arr2.pop();
    arr2.pop();
    arr2.pop();

    expect(arr1).toHaveLength(3);
    expect(arr2).toHaveLength(4);
  });

  test('7.It doesn\'t change empty array length', () => {
    const arr = new ArrayLike();
    arr.pop();
    arr.pop();
    arr.pop();

    expect(arr).toHaveLength(0);
  });

  test('8.Empty object recieve length property 0 when the method is used on this object with no arguments', () => {
    const obj = {};
    const arr = new ArrayLike();
    arr.pop.call(obj);

    expect(obj).toHaveLength(0);
  });

  test('9.It doesn\'t apply any arguments', () => {
    const arr = new ArrayLike();
    expect(arr.pop).toHaveLength(0);
  });

  test('10.It can be used on objects', () => {
    const arr = new ArrayLike();
    const obj = {
      0: 1,
      1: 2,
      length: 2
    };
    arr.pop.call(obj)

    expect(obj).toEqual({ 0: 1, length: 1 });
  });
});