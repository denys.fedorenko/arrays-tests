# JavaScript Unit Testing Project

Project in wich we write unit tests for some array methods

## Tests for array methods

This repository contains examples of tests for the following array methods:
 <br><b>-push;
 <br><b>-pop;
 <br><b>-forEach;
 <br><b>-sort;
 <br><b>-splice;
 <br><b>-filter.

 ## Installation

 Clone this repository 
 ```html
 git clone git@gitlab.com:denys.fedorenko/arrays-tests.git
 ```

 Go to directory
 ```html
 cd arrays-tests/
 ```

 Download list of packages required for a project with the correct versions 
 ```html
 npm install 
 ```

 Run tests
 ```html
 npm test 
 ```