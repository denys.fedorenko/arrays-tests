const ArrayLike = require('./index');

describe('Tests for method filter', () => {
  test('1.Instance has not own property filter', () => {
    const arr = new ArrayLike();

    expect(arr.hasOwnProperty('filter')).toBeFalsy();
  });

  test('2.Instance has method filter', () => {
    const arr = new ArrayLike();

    expect(arr.filter).toBeInstanceOf(Function);
  });

  test('3.It provides 3 arguments to callback function', () => {
    const arr = new ArrayLike(1, 2, 3);
    mockCallback = jest.fn();
    arr.filter(mockCallback);

    expect(mockCallback.mock.calls[0]).toHaveLength(3);
    expect(mockCallback.mock.calls[1]).toHaveLength(3);
    expect(mockCallback.mock.calls[2]).toHaveLength(3);
  });

  describe('Arguments in callback function', () => {
    test('3.1.First argument is currentValue', () => {
      expect(mockCallback.mock.calls[0][0]).toBe(1);
      expect(mockCallback.mock.calls[1][0]).toBe(2);
      expect(mockCallback.mock.calls[2][0]).toBe(3);
    });

    test('3.2.Second argument is currentIndex', () => {
      expect(mockCallback.mock.calls[0][1]).toBe(0);
      expect(mockCallback.mock.calls[1][1]).toBe(1);
      expect(mockCallback.mock.calls[2][1]).toBe(2);
    });

    test('3.3.Third argument is an array', () => {
      expect(mockCallback.mock.calls[0][2]).toEqual(new ArrayLike(1, 2, 3));
      expect(mockCallback.mock.calls[1][2]).toEqual(new ArrayLike(1, 2, 3));
      expect(mockCallback.mock.calls[0][2]).toEqual(new ArrayLike(1, 2, 3));
    });
  });

  test('4.It uses second argument as this', () => {
    const arr = new ArrayLike(1, 2, 3);
    const obj = { data: 2 };
    const callback = jest.fn(function (el) { return this.data > el});
    const result = arr.filter(callback, obj);

    expect(result).toHaveLength(1);
    expect(result[0]).toBe(1);
  });

  test('5.Returned value is an instance of array', () => {
    const arr = new ArrayLike(1, 2, 3);
    const mockCallback = jest.fn(() => false);
    const result = arr.filter(mockCallback);

    expect(result).toBeInstanceOf(ArrayLike);
  });

  test('6.It returns new array', () => {
    const arr = new ArrayLike(1, 2, 3);
    const callback = jest.fn(() => true);
    const result = arr.filter(callback);

    expect(result).not.toBe(arr);
  });

  test('7.It does not mutate the array on which it is called', () => {
    const arr = new ArrayLike('apple', 'banana', 'grapes');
    arr.filter((el) => el.length < 6);

    expect(arr).toEqual(new ArrayLike('apple', 'banana', 'grapes'));
  }); 

  test('8.It returns empty array when all elements fail test', () => {
    const arr = new ArrayLike(1, 2, 3);
    const result = arr.filter(() => false);

    expect(result).toHaveLength(0);
  });
  
  test('9.It includes elements in array, when callback returns true', () => {
    const arr = new ArrayLike(1, 2, 3);
    const callback = jest.fn();
    callback.mockReturnValueOnce(true)
            .mockReturnValueOnce(false)
            .mockReturnValueOnce(true);
    const result = arr.filter(num => callback(num));

    expect(result).toHaveLength(2);
  });

  test('10.It returns initial array when callback function changes array in procces', () => {
    const arr = new ArrayLike(1, 2, 3);
    const callback = jest.fn(() => { arr.push('some stuff'); return true });
    const result = arr.filter(callback);

    expect(result).toEqual(ArrayLike(1, 2, 3));
  });

  test('11.It works with new values of elements if they had been changed during the iteration', () => {
    const arr = new ArrayLike(2, 3, 4);
    const callback = jest.fn((el, index) => { arr[index + 1] = '10'; return true });
    const result = arr.filter(callback);

    expect(result[0]).toBe(2);
    expect(result[1]).toBe('10');
    expect(result[2]).toBe('10');
  }); 

  test('12.It throws error when cb is undefined', () => {
    const arr = new ArrayLike(1, 2, 3);

    expect(() => { arr.filter() }).toThrow(new TypeError('undefined is not a function'));
  });

  test('13.It doesnt work with empty array', () => {
    const arr = new ArrayLike(5);
    const callback = jest.fn();
    const result = arr.filter(callback);

    expect(result).toHaveLength(0);
  });

  test('14.Callback is not executed on deleted items in array', () => {
    const arr = new ArrayLike(1, 2, 3, 4, 5);
    const callback = jest.fn(() => { arr.pop(); return true });
    const result = arr.filter(callback);

    expect(result[0]).toBe(1);
    expect(result[1]).toBe(2);
    expect(result[2]).toBe(3);
  });

  test('15.Callback is not executed on empty array items', () => {
    const arr = new ArrayLike(5);
    const callback = jest.fn(() => true);
    arr.filter(callback);

    expect(callback).toHaveBeenCalledTimes(0);
  });

  test('16.The length property of filter method is 1', () => {

    expect(ArrayLike.prototype.filter).toHaveLength(1);
  });
});